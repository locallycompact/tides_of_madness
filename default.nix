{ pkgs ? import <nixpkgs> {} }:

with pkgs.idrisPackages;

build-idris-package rec {
  name = "app";
  version = "0.1";

  idrisDeps = [ containers contrib effects lens];

  extraBuildInputs = [];

  src = ./.;

  meta = {
    description = "My Idris App";
  };
}
