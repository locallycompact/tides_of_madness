{
  inputs.userpkgs.url = github:locallycompact/nixpkgs/69bd38185ab796c8aaf2594c31206cea7a2b7cd4;
  inputs.flake-utils.url = github:numtide/flake-utils;
  outputs = { self, userpkgs, flake-utils } :
    flake-utils.lib.eachSystem [ "x86_64-linux" ] (system:
      let pkgs = import userpkgs {inherit system; };
          p = (pkgs.idris2.withPkgs (ps: [ ps.comonad ps.sdl ps.idrall]) );
      in {
        devShell = pkgs.mkShell {
          buildInputs = [ p  ];
          shellHook=''
            export LD_LIBRARY_PATH="${p}/idris2-0.3.0/sdl-0.1.0/lib:$LD_LIBRARY_PATH"
            '';
        };
      }
    );
}
