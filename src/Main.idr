module Main

import Data.So
import Data.Vect
import Data.Vect.Elem
import Data.HVect
import Control.Monad.State


data Suit = Race | Location | OuterGod | GreatOldGod | Manuscript

Eq Suit where
  Race == Race = True
  Location == Location = True
  OuterGod == OuterGod = True
  GreatOldGod == GreatOldGod = True
  Manuscript == Manuscript = True
  _ == _ = False

Show Suit where
  show Race = "Race"
  show Location = "Location"
  show OuterGod = "OuterGod"
  show GreatOldGod = "GreatOldGod"
  show Manuscript = "Manuscript"

data Madness : Type where
  MkMadness : Nat -> Madness

data Score = MkScore Nat

Cast Score Nat where
  cast (MkScore x) = x

Show Score where
  show (MkScore x) = show x

Semigroup Score where
  (<+>) (MkScore x) (MkScore y) = MkScore (x + y)

data Name = MkName String

Show Name where
  show (MkName x) = x

data Phase = Init | Deal | Refresh | Draw | Swap | Evaluate | End

RoundNo : Type
RoundNo = Fin 2

data Round : Type where
  MkRound : Nat -> Round

Num Round where

data Player : Type where
    MkPlayer : Name -> Player

data Card : Suit -> Madness -> Type where
  MkCard : String -> Card s a

data PhaseF : Round -> Phase -> Type where
  MkPhaseF : (n : Round) -> (p : Phase) -> PhaseF n p

{--
data Tides : (Type -> Type) -> Type -> Type where
  NewGame : Player -> Player -> Tides m (PhaseF 0 Init)

  Shuffle : (1 s : PhaseF k Init) -> Tides m (PhaseF k Deal)

  DealT : (1 s : PhaseF k Deal) -> Tides m (PhaseF k Draw)

  DrawT : (1 s : PhaseF k Draw) -> Tides m (Res Bool (\end => PhaseF k (if end then Evaluate else Swap)))

  SwapT : (1 s : PhaseF k Swap) -> Tides m (PhaseF k Draw)

  EndRound : (nextRound : Round -> Round) -> (1 s : PhaseF k Evaluate) -> Tides m (Res Bool (\end => (if end then PhaseF k End else PhaseF (nextRound k) Init)))
  --}
data Stage = HGameInit | HGameStart | HGameComplete | HGameEnd

data H : Stage -> Stage -> Type where
  NewGame  : H HGameInit     HGameStart
  PlayGame : H HGameStart    HGameComplete
  EndGame  : H HGameComplete HGameEnd
  Compose'' : H a b -> H b c -> H a c


data K : Phase -> Phase -> Type where
  StartRound   : K Init Deal
  DealCards    : K Deal Draw
  EndRound'    : K Draw End
  Compose' : K a b -> K b c -> K a c

compose : K a b -> K b c -> K a c

data FreeCat : (k -> k -> Type) -> k -> k -> Type where
  Id : FreeCat p a a
  Compose : FreeCat p a b -> FreeCat p b c -> FreeCat p a c

infixr 4 >=>
(>=>) : K a b -> K b c -> K a c
(>=>) = Compose'

infixr 4 >==>
(>==>) : H a b -> H b c -> H a c
(>==>) = Compose''

Game : K Init End
Game = StartRound >=> DealCards >=> EndRound'

{--
data Rec : (u -> v) -> Vect n u -> Type where
      RNil : Rec f []
      DN : (f x) -> Rec f xs -> Rec f (x :: xs)
      --}
data CoRec : (u -> v) -> Vect n u -> Type where
      CoVal : Elem r rs => (f r) -> CoRec f rs

A : Type
A = CoRec (K Init) [End]

B : A
B = CoVal Game

{--
interpretH : {n : Nat } -> H a b -> Vect n Type
interpretH {n=0} NewGame = Nil
interpretH {n=1} PlayGame = [K Init End]
interpretH {n=0} EndGame = Nil
interpretH (Compose'' x y) = interpretH x ++ interpretH y
--}
PlayThrough : H HGameInit HGameEnd
PlayThrough = NewGame >==> PlayGame >==> EndGame

{--
interpretH' : ( p : H a b) -> HVect (interpretH p)
interpretH' NewGame = Nil
interpretH' PlayGame = Game
interpretH' EndGame = Nil
interpretH' (Compose'' x y) = interpretH' x ++ interpretH' y
--}

interpretK : HasIO m => K a b -> m ()
interpretK StartRound = putStrLn "Starting Round"
interpretK DealCards = putStrLn "Dealing Cards"
interpretK EndRound' = putStrLn "Ending Round"
interpretK (Compose' x y) = interpretK x >> interpretK y


interpretH' : HasIO m => (p : H a b) -> m ()
interpretH' NewGame = putStrLn "Starting Game"
interpretH' PlayGame = interpretK Game
interpretH' EndGame = putStrLn "Ending Game"
interpretH' (Compose'' x y) = interpretH' x >> interpretH' y

{--
runFreeCat : Monad m => m k -> (p x y -> m k) -> FreeCat p a b -> m k
runFreeCat i _ Id = i
runFreeCat i f (Compose g h) = do
   runFreeCat i f g
   runFreeCat i f h
   --}
main : IO ()
main = interpretH' PlayThrough

{--
interpretTides : Tides m a -> State (List ()) a
interpretTides x = case x of
  NewGame x y => 
  Shuffle 

mutual

  interface MenuIO (m : Type -> Type) where

    initIO : ST m Var [ add (State (Vect 0 Player)) ]

    createPlayer : Name -> (ps : Var) -> ST m () [ ps ::: State (Vect n Player) :-> State (Vect (S n) Player) ]

    runGame : ST m () [ ps ::: State (Vect n Player) ]


  interface GameIO (m : Type -> Type) where
    PhaseF : Round -> Phase -> Type

    newGame : {xs : Vect n Type}
           -> Player
           -> Player
           -> ST m Var [ add (Composite [ PhaseF (MkRound 0) Init
                                        , State (PlayerI [] [])
                                        , State (PlayerI [] [])
                                        , State (CVect xs) ]) ]

    shuffleDeck : { xs : Vect n Type }
               -> { ys : Vect n Type }
               -> ST m () [ comp ::: Composite [ PhaseF (MkRound 0) Init
                                               , State (PlayerI [] [])
                                               , State (PlayerI [] [])
                                               , State (CVect xs)]
                                 :-> Composite [ PhaseF (MkRound 0) Deal
                                               , State (PlayerI [] [])
                                               , State (PlayerI [] [])
                                               , CVect ys]]

    dealCards : { k : Round }
             -> { xs : Vect ((2*cast k) + n) Type }
             -> { ys : Vect n Type }
             -> { pl_hand : Vect (cast k) Type }
             -> { op_hand : Vect (cast k) Type }
             -> ST m () [ comp ::: Composite [ PhaseF k Deal
                                             , State (PlayerI [] [])
                                             , State (PlayerI [] [])
                                             , CVect xs]
                               :-> Composite [ PhaseF k Draw
                                             , State (PlayerI pl_hand [])
                                             , State (PlayerI op_hand [])
                                             , CVect ys]]

  EvalPhaseScoringEffect : Type
  EvalPhaseScoringEffect = { m : Type -> Type }
                        -> (ConsoleIO m, GameIO m)
                        => { k : Round }
                        -> { pl_field : Vect (cast k + 5) Type }
                        -> { op_field : Vect (cast k + 5) Type }
                        -> ( comp : Var )
                        -> ST m () [ comp ::: Composite [ PhaseF {m} k Evaluate
                                                        , State (PlayerI [] pl_field)
                                                        , State (PlayerI [] op_field)]]

  data Card : Suit -> Madness -> Type where
    MkCard : String -> EvalPhaseScoringEffect -> Card s a

  data CVect : Vect n Type -> Type where
    Nil : CVect []
    (::) : Card s m -> CVect ts -> CVect (Card s m :: ts)

  data Player : Type where
    MkPlayer : Name -> Player

  data PlayerI : ( xs : Vect m Type ) -> ( ys : Vect n Type ) -> Type where
    MkPlayerI : Player -> Score -> Madness -> CVect xs -> CVect ys -> PlayerI xs ys

addPoints : ConsoleIO m => Score -> (pl : Var) -> ST m () [pl ::: State (PlayerI [] pl_field)]
addPoints pts pl = do
    MkPlayerI (MkPlayer name) _ _ _ _ <- read pl
    putStrLn $ "Awarding " <+> show pts <+> " points to Player " <+> (show name )
    update pl (\(MkPlayerI a x m h f) => MkPlayerI a (x <+> pts) m h f)

cardHasSuit : Suit -> Card s m -> Bool
cardHasSuit x _ = s == x

filterSuit : Suit -> { xs : Vect n Type} -> ( as : CVect xs ) -> (p : Nat ** ys : Vect p Type ** CVect ys )
filterSuit s Nil = (_ ** _ ** Nil)
filterSuit s (t :: ts) with (filterSuit s ts)
  | ( _ ** _ ** ys') = if (cardHasSuit s t) then ( _ ** _ ** t :: ys' ) else ( _ ** _ ** ys' )

replicate : (n : Nat )  -> a -> HVect (replicate n a)
replicate Z _ = []
replicate (S n) x = x :: replicate (n) x

length : CVect xs -> Int
length Nil = 0
length (x :: xs) = 1 + length xs

countSuit : Suit -> CVect xs -> Nat
countSuit s xs = (\(p ** _ ** _) => p) $ filterSuit s xs

majoritySuitEffect : Suit -> Score -> EvalPhaseScoringEffect
majoritySuitEffect s n = \comp => do
  [ph, pl, op] <- split comp
  MkPlayerI (MkPlayer n1) _ _ _ xs <- read pl
  MkPlayerI (MkPlayer n2) _ _ _ ys <- read op
  let x = countSuit s xs
  let y = countSuit s ys
  putStrLn $ "Player " <+> show n1 <+> " has " <+> show x <+> (show s)
  putStrLn $ "Player " <+> show n2 <+> " has " <+> show y <+> (show s)
  if x > y then call (addPoints n pl) else pure ()
  combine comp [ph, pl, op]

PnakoticManuscripts : Card Manuscript (MkMadness 1)
PnakoticManuscripts = MkCard "Phakotic Manuscripts" $ majoritySuitEffect OuterGod (MkScore 7)

runCard : ConsoleIO m => Card s a -> EvalPhaseScoringEffect
runCard (MkCard x f) = f

--}
